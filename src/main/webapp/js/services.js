'use strict';

/* Services */

var services = angular.module('services', ['ngResource']);

services.factory('LoginService', function($resource) {
    return $resource('api/authenticate', {}, {
        authenticate: {
            method: 'POST',
            url : 'api/authenticate',
//            params: {'action' : 'api/authenticate'},
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }
    });
});