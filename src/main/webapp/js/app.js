'use strict';

/* App Module */

var xAuthTokenHeaderName = 'x-auth-token';

$('.ui.pointing.dropdown').dropdown();

var todoApp = angular.module('todoApp', [
	'ngRoute',
    'ngCookies',
    'ngAnimate',
    'services',
	'todoControllers'
]);

todoApp.config(['$routeProvider', '$locationProvider', '$httpProvider', '$animateProvider',
    function($routeProvider, $locationProvider, $httpProvider) {

        $routeProvider.
            when('/home', {
                templateUrl: 'partials/home.html',
                controller: 'HomeCtrl'
            })
            .when('/about', {
                templateUrl: 'partials/about.html'
            })
            .when('/create', {
                templateUrl: 'partials/create.html',
                controller: 'HomeCtrl'
            })
            .when('/entries/:listId', {
                templateUrl: 'partials/entries.html',
                controller: 'EntryController'
            })
            .when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'LoginController'
            })
            .otherwise({
                redirectTo: '/home'
            });

        /* Intercept http errors */
        var interceptor = function ($rootScope, $q, $location) {

            function success(response) {
                return response;
            }

            function error(response) {

                var status = response.status;
                var config = response.config;
                var method = config.method;
                var url = config.url;

                switch (status) {
                    case 401: {
                        $location.path( "/login" );
                    } break;
                    case 403: {
                        $rootScope.error = "Bad credentials";
                    } break;
                    default: {
                        $rootScope.error = method + " on " + url + " failed with status " + status;
                    }
                }
                return $q.reject(response);
            }

            return function (promise) {
                return promise.then(success, error);
            };
        };
        $httpProvider.responseInterceptors.push(interceptor);
    }
]);

todoApp.run(function($rootScope, $http, $location, $cookieStore, LoginService) {

    /* Reset error when a new view is loaded */
    $rootScope.$on('$viewContentLoaded', function() {
        delete $rootScope.error;
    });

    $rootScope.hasRole = function(role) {

        if ($rootScope.user === undefined) {
            return false;
        }

        if ($rootScope.user.roles[role] === undefined) {
            return false;
        }
        return $rootScope.user.roles[role];
    };

    $rootScope.logout = function() {
        delete $rootScope.user;
        delete $http.defaults.headers.common[xAuthTokenHeaderName];
        $cookieStore.remove('user');
        $location.path("/login");
    };

    /* Try getting valid user from cookie or go to login page */
    var originalPath = $location.path();
    $location.path("/login");
    var user = $cookieStore.get('user');
    if (user !== undefined) {
        $rootScope.user = user;
        $http.defaults.headers.common[xAuthTokenHeaderName] = user.token;

        $location.path(originalPath);
    }

    $rootScope.mainMenu = [
        {name:'Home', url:'/#/home', icon:'home'},
        {name:'About', url:'/#/about', icon:'info'}
    ];

    switch ($location.path()) {
        case '/home':
            $rootScope.selectedTab = 0;
            break;
        case '/about':
            $rootScope.selectedTab = 1;
            break;
        default:
            $rootScope.selectedTab = 0;
    }

    $rootScope.selectTab = function(row) {
        $rootScope.selectedTab = row;
    };
});