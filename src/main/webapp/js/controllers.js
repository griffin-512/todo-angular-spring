'use strict';

/* Controllers */

var todoControllers = angular.module('todoControllers', []);

todoControllers.controller('HomeCtrl', ['$rootScope', '$scope', '$routeParams', '$http',
                                '$location', '$cookieStore',
	function($rootScope, $scope, $routeParams, $http, $location, $cookieStore) {

        init();

        function init() {
            $http.get('/api/lists/' + $rootScope.user.name)
                .success(function (data, status, headers, config) {
                    $scope.todoList = data;
                })
                .error(function (data, status, headers, config) {
                    console.log('error: data = ', data);
                });
        }

        $scope.go = function (path) {
            $location.path(path);
        };

        $scope.removeTodo = function(todo){
            $http.delete('/api/lists/' + todo.id)
                .success(function (data, status, headers, config) {
                    console.log(data);
//                    if (data) {
//                        var index = $scope.todoList.indexOf(todo);
//                        $scope.todoList.splice(index, 1);
//                        $location.path("/home");
//                    }
                })
                .error(function (data, status, headers, config) {
                    console.log('error: data = ', data);
                });
            var index = $scope.todoList.indexOf(todo);
            $scope.todoList.splice(index, 1);
        };

        $scope.loadList = function () {
            $http.get('/api/list/' + $scope.listId)
                .success(function (data, status, headers, config) {
                    $scope.list = data;
                })
                .error(function (data, status, headers, config) {
                    console.log('error: data = ', data);
                });
        };

        $scope.createTodoList = function () {

            var user = $cookieStore.get('user');
            $http.post('/api/list', {
                "title": $scope.title,
                "description": $scope.description,
                "owner": user.name
            }).success(function(data, status, headers, config) {
                $scope.title = '';
                $scope.description = '';
                $scope.newListId = data;

                $location.path('/home');
            }).error(function(data, status, headers, config) {
                console.log('error: data = ' + data);
            });
        };
    }
]);

todoControllers.controller('EntryController', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {

        init($routeParams.listId);

        function init(listId) {
            $http.get('/api/entries/' + listId)
                .success(function(data, status, headers, config) {
                    $scope.entryList = data;
                })
                .error(function(data, status, headers, config) {
                    console.log('error: data = ' + data);
                });
        };
    }
]);

todoControllers.controller('LoginController', ['$scope', '$rootScope', '$location', '$http', '$cookieStore', 'LoginService',
    function($scope, $rootScope, $location, $http, $cookieStore, LoginService) {
        $scope.login = function() {
            LoginService.authenticate($.param({username: $scope.username, password: $scope.password}), function(user) {
                $rootScope.user = user;
                $http.defaults.headers.common[xAuthTokenHeaderName] = user.token;
                $cookieStore.put('user', user);
                $location.path("/");
            });
        };
    }
]);