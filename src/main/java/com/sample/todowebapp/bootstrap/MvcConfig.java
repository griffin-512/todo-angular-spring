package com.sample.todowebapp.bootstrap;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.sample.todowebapp.controllers")
public class MvcConfig extends WebMvcConfigurerAdapter {

}
