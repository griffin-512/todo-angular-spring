package com.sample.todowebapp.bootstrap;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by amarchuk on 11.06.2014.
 */

public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
    //do nothing
}