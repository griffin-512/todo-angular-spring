package com.sample.todowebapp.bootstrap;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */

@Configuration
@Import({DatabaseConfig.class, WebSecurityConfig.class})
@ComponentScan(basePackages = {"com.sample.todowebapp.dao"})
public class RootConfig {

}
