package com.sample.todowebapp.dao.impl;

import com.sample.todowebapp.dao.TodoEntryDao;
import com.sample.todowebapp.exceptions.TodoListNotFoundException;
import com.sample.todowebapp.model.TodoEntry;
import com.sample.todowebapp.model.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Marchuk on 08.06.2014.
 */
@Repository
public class TodoEntryDaoImpl implements TodoEntryDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<TodoEntry> todoListRowMapper = new RowMapper<TodoEntry>() {
        @Override
        public TodoEntry mapRow(ResultSet resultSet, int i) throws SQLException {
            TodoEntry entry =  new TodoEntry(
                    resultSet.getLong("id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getBoolean("isDone")
            );
            entry.setListId(resultSet.getLong("listId"));
            return entry;
        }
    };

    @Override
    public TodoEntry getTodoEntryById(Long id) {
        return new TodoEntry(new Long("1"), "new test entry", "simple todo entry", false);
    }

    @Override
    public void save(TodoEntry todoList) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<TodoEntry> getEntriesByListId(Long id) {
        List<TodoEntry> list = jdbcTemplate.query("select * from entries where listId = :id",
                todoListRowMapper, id);
        if (list.isEmpty()) {
            return new ArrayList<TodoEntry>();
        } else {
            return list;
        }
    }
}
