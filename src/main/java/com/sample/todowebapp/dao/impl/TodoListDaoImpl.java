package com.sample.todowebapp.dao.impl;

import com.sample.todowebapp.dao.TodoEntryDao;
import com.sample.todowebapp.dao.TodoListDao;
import com.sample.todowebapp.exceptions.TodoListNotFoundException;
import com.sample.todowebapp.model.TodoEntry;
import com.sample.todowebapp.model.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */

@Repository
public class TodoListDaoImpl implements TodoListDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TodoEntryDao todoEntryDao;

    private RowMapper<TodoList> todoListRowMapper = new RowMapper<TodoList>() {
        @Override
        public TodoList mapRow(ResultSet resultSet, int i) throws SQLException {
            return new TodoList(
                    resultSet.getLong("id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getString("owner"),
                    new ArrayList<TodoEntry>()
            );
        }
    };

    @Override
    public List<TodoList> getAll(String owner) {
        List<TodoList> list = jdbcTemplate.query("select * from lists where owner = :owner",
                todoListRowMapper, owner);
        if (list.isEmpty()) {
            throw new TodoListNotFoundException("Not TODO list found for owner = " + owner);
        } else {
            for (TodoList todoList : list) {
                List<TodoEntry> entries = new ArrayList<TodoEntry>();
                entries.addAll(todoEntryDao.getEntriesByListId(todoList.getId()));
                todoList.setEntries(entries);
            }
            return list;
        }
    }

    @Override
    public TodoList getTodoListById(final Long id) {

        List<TodoList> list = jdbcTemplate.query("select * from lists where id = :id",
                todoListRowMapper, id);
        if (list.isEmpty()) {
            throw new TodoListNotFoundException("Not TODO list found for id = " + id);
        } else {
            TodoList todoList = list.get(0);
            List<TodoEntry> entries = new ArrayList<TodoEntry>();
            entries.addAll(todoEntryDao.getEntriesByListId(todoList.getId()));
            todoList.setEntries(entries);
            return todoList;
        }
    }

    @Override
    public void save(final TodoList todoList) {

        final PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps =
                        connection.prepareStatement("INSERT INTO lists (title, description, owner) VALUES (?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, todoList.getTitle());
                ps.setString(2, todoList.getDescription());
                ps.setString(3, todoList.getOwner());
                return ps;
            }
        };

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);
        Long newId = keyHolder.getKey().longValue();

        // populate the id
        todoList.setId(newId);
    }

    @Override
    public Boolean delete(Long id) {
        return jdbcTemplate.update("DELETE FROM lists WHERE id = :id", id) > 0 ? true : false;
    }
}
