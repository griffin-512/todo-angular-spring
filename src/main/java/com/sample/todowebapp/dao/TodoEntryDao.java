package com.sample.todowebapp.dao;

import com.sample.todowebapp.model.TodoEntry;

import java.util.List;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */
public interface TodoEntryDao {
    TodoEntry getTodoEntryById(Long id);
    List<TodoEntry> getEntriesByListId(Long id);

    void save(TodoEntry todoList);
    void delete(Long id);
}
