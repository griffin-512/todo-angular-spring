package com.sample.todowebapp.dao;

import com.sample.todowebapp.model.TodoEntry;
import com.sample.todowebapp.model.TodoList;

import java.util.List;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */
public interface TodoListDao {
    List<TodoList> getAll(String user);
    TodoList getTodoListById(Long id);

    void save(TodoList todoList);
    Boolean delete(Long id);
}
