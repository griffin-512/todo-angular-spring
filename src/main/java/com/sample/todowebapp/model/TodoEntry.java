package com.sample.todowebapp.model;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */
public class TodoEntry {

    private Long id;
    private Long listId;
    private String title;
    private String description;
    private Boolean isDone;

    public TodoEntry() {
    }

    public TodoEntry(Long id, String title, String description, Boolean isDone) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isDone = isDone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }
}
