package com.sample.todowebapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */
public class TodoList {

    private Long id;
    private String title;
    private String description;
    private String owner;
    private List<TodoEntry> entries = new ArrayList<TodoEntry>();

    public TodoList() {

    }

    public TodoList(Long id, String title, String description, String owner, List<TodoEntry> entries) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.owner = owner;
        this.entries = entries;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<TodoEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<TodoEntry> entries) {
        this.entries = entries;
    }
}
