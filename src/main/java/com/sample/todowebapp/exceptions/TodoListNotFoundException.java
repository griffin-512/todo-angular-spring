package com.sample.todowebapp.exceptions;

/**
 * Created by Alexander Marchuk on 08.06.2014.
 */
public class TodoListNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TodoListNotFoundException() {
        super();
    }

    public TodoListNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TodoListNotFoundException(String message) {
        super(message);
    }

    public TodoListNotFoundException(Throwable cause) {
        super(cause);
    }
}
