package com.sample.todowebapp.controllers;

import com.sample.todowebapp.dao.TodoListDao;
import com.sample.todowebapp.exceptions.TodoListNotFoundException;
import com.sample.todowebapp.model.TodoEntry;
import com.sample.todowebapp.model.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Alexander Marchuk on 07.06.2014.
 */

@RestController
public class TodoListController {

    @Autowired
    private TodoListDao todoListDao;

    @RequestMapping("list/{id}")
    public TodoList getTodoListById(@PathVariable Long id) {
        return todoListDao.getTodoListById(id);
    }

    @RequestMapping(value = "list", method = RequestMethod.POST)
    public Long createTodoList(@RequestBody TodoList todoList) {
        todoListDao.save(todoList);
        return todoList.getId();
    }

    @RequestMapping("lists/{username}")
    public List<TodoList> getTodoLists(@PathVariable String username) {
        return todoListDao.getAll(username);
    }

    @RequestMapping(value = "lists/{id}", method = RequestMethod.DELETE)
    public Boolean deleteList(@PathVariable("id") Long id) {
        return todoListDao.delete(id);
    }

    @RequestMapping("entries/{listId}")
    public List<TodoEntry> getEntryLists(@PathVariable Long listId) {
        return todoListDao.getTodoListById(listId).getEntries();
    }

    @ExceptionHandler(TodoListNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleTodoListNotFoundException(TodoListNotFoundException e) {
        return e.getMessage();
    }
}
